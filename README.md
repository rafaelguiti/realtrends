# INSTALAÇÃO E EXECUÇÃO

Para instalar a aplicação, basta entrar na pasta do projeto e executar:

``docker-compose up -d``

Terminada a criação da imagem e do container, a aplicação ficará disponível na porta 8000 (ex. http://localhost:8000)

# Testes

Com o container em execução, rodar o seguinte comando para executar os testes:

``docker exec realtrendstest python manage.py test``
 