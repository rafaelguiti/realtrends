import requests
from django.shortcuts import render


def ml_handler():
    category = "MLA420040"
    limit = 50
    offset = 0
    results = []
    while True:
        url = f"https://api.mercadolibre.com/sites/MLA/search?offset={offset}&category={category}"
        requests.get(url).json()
        ml_response = requests.get(url).json()
        if 'results' in ml_response:
            results += ml_response['results']
        offset += limit
        if not 'paging' in ml_response or offset > ml_response['paging']['total'] - limit:
            break
    return results


def ml_handler_seller_nickname(id):
    url = f"https://api.mercadolibre.com/sites/MLA/search?seller_id={id}"
    ml_response = requests.get(url).json()
    if 'seller' in ml_response and 'nickname' in ml_response['seller']:
        return ml_response['seller']['nickname']


def get_items(order_by):
    items = [];
    results = ml_handler()
    if results:
        results = sorted(results, key=lambda item: item[order_by], reverse=True)
        for index, item in enumerate(results[:10]):
            item.update({'nickname': ml_handler_seller_nickname(item['seller']['id'])})
            items.append(item)
    return items


def get_best_sellers(request):
    return render(request, 'index.html', {'items': get_items('sold_quantity'), 'title': 'Mais vendidas'})


def more_expensives(request):
    return render(request, 'index.html', {'items': get_items('price'), 'title': 'Mais caras'})
