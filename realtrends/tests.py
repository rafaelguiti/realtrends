import json
from django.test import SimpleTestCase, tag
from . import views
from django.shortcuts import reverse


class TestRealTrends(SimpleTestCase):
    def test_get_items_by_sold_quantity(self):
        """
        Testa a função get_items na view
        Espera retornar os 10 mais vendidos
        """
        items = views.get_items('sold_quantity')
        self.assertGreater(len(items), 0)
        self.assertIsNotNone(items[0]['id'])

    def test_ml_handler(self):
        """
        Testa a função ml_handler na view
        Pega todos os resultados para a categoria passada
        """
        items = views.ml_handler()
        self.assertGreater(len(items), 0)
        self.assertIsNotNone(items[0]['id'])

    def test_ml_handler_seller_nickname(self):
        """
        Testa pegar o nickname de um vendedor, passando o id dele
        """
        items = []
        with open('test.json') as json_file:
            items = json.load(json_file)
        self.assertIsNotNone(items)
        self.assertGreater(len(items), 0)
        self.assertIsNotNone(items[0]['seller'])
        self.assertIsNotNone(items[0]['seller']['id'])
        nickname = views.ml_handler_seller_nickname(items[0]['seller']['id'])
        self.assertIsNotNone(nickname)

    def test_get_best_sellers(self):
        """
        Testa a página dos maiores vendedores que está na index
        """
        response = self.client.get(reverse('best-sellers'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')
        self.assertContains(response, '<h3 class="text-muted" id="page-title">Mais vendidas</h3>')

    @tag('atual')
    def test_get_more_expensives(self):
        """
        Testa a página que apresenta os resultados mais caros
        """
        response = self.client.get(reverse('more-expensives'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')
        self.assertContains(response, '<h3 class="text-muted" id="page-title">Mais caras</h3>')
